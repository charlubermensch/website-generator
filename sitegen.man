.TH WEBSITE-GENERATOR 7 "15 February 2022"
.SH NAME
website-generator \- Commands with processing Monero payments as purpose
.SH SYNOPSIS
.SY python
.OP --online
.OP --verbose
.OP .
.YS
.SH OPTIONS
.TP
.B --verbose
.br
Be more verbose.
.TP
.B --online
.br
Configure the output to be compatible with being online rather than offline.
.SH DEPENDENCIES
Python and bs4
.SH "REPORTING BUGS"
GitLab issues: <https://gitlab.com/charlubermensch/website-generator/-/issues>
.SH "SEE ALSO"
python(1),rsync,nginx,chmod,bs4
.SH AUTHOR
Written by "Charl'Ubermensch" (pseudonyme).
.LP
.UR https://gitlab.com/charlubermensch
Remote Git repositories
.UE
.LP
.MT inlife.developper@tutanota.com
CharlUbermensch
.ME
.br
.MT contact@charlubermensch.com
CharlUbermensch
.ME
.SH COPYRIGHT
License GPLv2: GNU General Public License version 2.
<https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html>
.PP
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
