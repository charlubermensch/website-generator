# Website generator

Generate webpages for NGINX or alike with footer, header and CSS file.

The CSS changes for every pages with only the tags used in order to decrease bandwidth use.

# Getting started

Write your webpages in a folder named `input`, give them the path `path/index.html` (will become https://yourwebsite.org/path)

Then run:

```sh
python . "*"
```

It'll generate `output`. You can try to see the result with any browser by opening the `index.html` files in this directory.

# Puting it online

Run:

```sh
python . --online "*"
```

Then one can use rsync to send `output` to one's server.

For me I do:

```sh
rsync -rv output/* inlife@charlubermensch.com:/var/www/charlubermensch
```

NB: do not forget the "-r", otherwise it'd skip directories. Nor shall you for the trailing "/\*", otherwise it may add an `output` directory.

If you want to connect not as root like I do, you may have to change the permission (security issue but I think less than connecting as root):

```sh
chmod 777 /var/www/charlubermensch
```

# Dependencies

- bs4 (BeautifulSoup4)
- Python

# Roadmap

- [x] offline and online
- [x] refactored
- [x] generate titles
- [ ] adapt to other websites
- [ ] add help and version argv
- [ ] decrease code size

# License

The license is the Gnu General License Version 2. This code shouldn't be used in proprietary software.
