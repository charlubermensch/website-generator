"""Generate CSS & HTML from markdown."""

import sys

from src.py.folder import run_pages, get_pages

from os import listdir, getcwd

def run():
    """Run the script."""
    if "input" not in listdir(getcwd()):
        print("error: no folder named \"input\"", file=sys.stderr)
        sys.exit(1)

    pages = get_pages("input")

    if "--verbose" in sys.argv:
        for page in pages:
            print("page %s" % page)

    if not pages:
        print("input is empty")
        sys.exit(0)

    run_pages(pages)


if __name__ == "__main__":
    run()
