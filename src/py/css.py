"""Generate CSS."""

from json import loads

from bs4 import BeautifulSoup


def get_json() -> dict:
    """Get the JSON CSS file content."""
    with open("src/css/css.json", "rb") as file:
        b_file = file.read()
    s_file = str(b_file, encoding="utf-8")
    return loads(s_file)


def get_articles_css(html: str) -> str:
    """Get the CSS that formats the tags used in article."""
    soup = BeautifulSoup(html, features="html.parser")

    ret = ""

    json = get_json()
    for key in json.keys():
        all_found = soup.find_all(key)
        if all_found:
            ret += "%s {\n%s\n}\n\n" % (key, json[key])

    return ret


def get_raw_base_css() -> str:
    """Get the raw content of `src/css/base.css`."""
    with open("src/css/base.css", "rb") as file:
        b_file = file.read()
    return str(b_file, encoding="utf-8")


def get_commentless(file_content: str) -> str:
    """Return `file_content` without comments."""
    output = ""
    for line in file_content.split("\n"):
        if not line.startswith("/*") and not line.endswith("*/"):
            output += line + "\n"
    return output.replace("\n\n\n", "\n\n")


def get_base_css() -> str:
    """Get the base of CSS."""
    file_content = get_raw_base_css()
    return get_commentless(file_content)


def get_css(html: str) -> str:
    """Return the CSS."""
    css = get_base_css() + "\n"
    css += get_articles_css(html)
    return css
