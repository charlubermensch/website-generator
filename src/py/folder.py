"""List folders and "run" them."""

import sys

from os import listdir, getcwd, mkdir
from os.path import isdir, isfile, basename, dirname, splitext

from src.py.html import get_html
from src.py.css import get_css


def make_folder(folder: str):
    """Recursively use `os.mkdir()` to build `folder`."""
    while True:
        try:
            mkdir(folder)
        except FileNotFoundError:
            make_folder(dirname(folder))
        else:
            break


def run_pages(pages: list):
    """Run the script on `pages`."""
    for page in pages:
        run_page(page)


def check_folder(folder: str):
    """Check `folder` exists and if not makes it."""
    if not isdir("output/" + folder):
        try:
            make_folder("output/" + folder)
        except OSError as error:
            err_msg = "error mkdir \"%s\"" % ("output/" + folder)
            print(err_msg, file=sys.stderr)
            print("error_message %s" % error, file=sys.stderr)
            sys.exit(1)
        if "--verbose" in sys.argv:
            print("mkdir %s" % ("output/" + folder))


def run_page(page: str):
    """Run the script on `page`."""
    assert page.startswith("input/"), "page has to start with \"input/\""

    if not isfile(page):
        return

    page = page[6:]

    directory = dirname(page)

    directory = "" if not directory else directory + "/"
    if basename(page) != "index.html":
        directory += splitext(basename(page))[0] + "/"

    if directory:
        check_folder(directory)

    html = get_html("input/" + page)
    with open("output/" + directory + "index.html", "wb") as file:
        file.write(bytes(html, encoding="utf-8"))

    if "--verbose" in sys.argv:
        print("file %s" % ("output/" + directory + "index.html"))

    css = get_css(html)
    with open("output/" + directory + "style.css", "wb") as file:
        file.write(bytes(css, encoding="utf-8"))

    if "--verbose" in sys.argv:
        print("file %s" % ("output/" + directory + "style.css"))


def is_garbage(path: str) -> bool:
    """Return if `path` should be skipped."""
    if not isdir(path):
        return True

    if path in ("output", "input"):
        return True

    return basename(path).startswith(".") or basename(path).startswith("__")


def is_webpage_folder(path: str) -> bool:
    """Return if `path` is a webpage folder."""
    return "index.html" in listdir(path)


def get_path(folder: str, file: str) -> str:
    """Return the proper relative path for `file`."""
    if folder == getcwd():
        return file

    return folder + "/" + file


def get_pages(folder: str) -> list:
    """Return the folders in `folder` (sub-folders too)."""
    if "--verbose" in sys.argv:
        print("folder %s" % folder)

    pages = []

    for file in listdir(folder):
        path = folder + "/" + file

        if isdir(path):
            pages += get_pages(path)
            continue

        if path.endswith(".html"):
            pages += [path]

    return pages
