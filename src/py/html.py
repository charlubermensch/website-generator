"""Generate the html pages."""

import sys

from os import getcwd
from os.path import dirname

from bs4 import BeautifulSoup


def get_src(file_name: str):
    """Get `file` in `"src"` folder."""
    with open("src/" + file_name, "rb") as file:
        file_bytes = file.read()
    return str(file_bytes, encoding="utf-8")


def get_body(body_path: str) -> str:
    """Return the body part of the html."""
    with open(body_path, "rb") as file:
        return str(file.read(), encoding="utf-8")


def get_good_body(body_path: str) -> str:
    """Return the body with good intentation."""
    body = get_body(body_path)

    if body.startswith("NOINDENT"):
        return body[8:]

    good_body = ""
    for line in body.split('\n'):
        good_body += '\t\t\t' + line + '\n'

    return good_body[:-1]


def get_corrected_folder(folder: str) -> str:
    """Return the folder of output not input."""
    if folder in (" ", ".", "/var/www/charlubermensch"):
        return ""

    return folder[6:]


def get_root() -> str:
    """Return the root of the website."""
    if "--online" in sys.argv:
        return "https://charlubermensch.com"
    return getcwd() + "/output"


def get_home():
    """Return link to home."""
    if "--online" in sys.argv:
        return "https://charlubermensch.com"
    return getcwd() + "/output/index.html"


def get_offline_favicon() -> str:
    """Return html for offline favicon if offline."""
    if "--online" in sys.argv:
        return ""
    return '<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">'


def get_donate_link() -> str:
    """Return link to donate page."""
    if "--online" in sys.argv:
        return "https://charlubermensch.com/donate"
    return "donate/index.html"


def get_title(body: str) -> str:
    """Return title."""
    soup = BeautifulSoup(body, "html.parser")

    tags = ("h1", "h2", "h3", "p")

    for tag in tags:
        found = soup.find_all(tag)
        if found:
            cleaned = [element for element in found if element.text]
            return cleaned[0].text

    return "Charl'Übermensch —— empty article"


def get_header(folder: str, body: str) -> str:
    """Return the header with %s replaced by good vars."""
    root = get_root()
    home = get_home()
    basic_title = "Charl'Übermensch's webpage"
    return get_src("html/header.html") % (
            (basic_title if folder == "input" else get_title(body)),
            get_offline_favicon(),
            home,
            root,
            get_donate_link()
    )


def get_html(body_path: str) -> str:
    """Return the HTML."""
    body = get_good_body(body_path).strip()
    folder = dirname(body_path)
    html = get_header(folder, body)

    html += "\t\t<div id=\"article\">\n"
    html += "\t\t\t" + body
    html += "\t\t</div>\n"
    html += get_src("html/footer.html")
    html += "\n\t</body>\n"
    html += "</html>"

    return html
